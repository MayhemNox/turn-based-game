import random


class Game:
    def __init__(self):
        self.class1 = "Paladin"
        self.class2 = "Crusader"
        self.class3 = "Holy Warrior"
        self.class4 = "Dragon Shaman"
        self.class1hp = 100
        self.class2hp = 100
        self.class3hp = 100
        self.class4hp = 100

        self.class1attacks = {
            "Low attack": random.choice(range(15, 20)),
            "Medium attack": random.choice(range(10, 30)),
            "Heavy attack": random.choice(range(5, 40)),
            "Heal": int(random.choice(range(10, 50)) + self.class1hp)
        }

        self.class2attacks = {
            "Low attack": random.choice(range(15, 20)),
            "Medium attack": random.choice(range(10, 30)),
            "Heavy attack": random.choice(range(5, 40)),
            "Heal": int(random.choice(range(10, 50)) + self.class2hp)
        }

        self.class3attacks = {
            "Low attack": random.choice(range(15, 20)),
            "Medium attack": random.choice(range(10, 30)),
            "Heavy attack": random.choice(range(5, 40)),
            "Heal": int(random.choice(range(10, 50)) + self.class3hp)
        }

        self.class4attacks = {
            "Low attack": random.choice(range(15, 20)),
            "Medium attack": random.choice(range(10, 30)),
            "Heavy attack": random.choice(range(5, 40)),
            "Heal": int(random.choice(range(10, 50)) + self.class4hp)
        }
        self.user1pick()
        self.user2pick()
        self.Ceva()

    def user1pick(self):
        print("\nPlayer 1 turn\n")
        try:
            user1 = int(input("Choose which class you want to play: \n1) {}\n2) {}\n3) {} \n4) {} \n\n".format(self.class1, self.class2, self.class3, self.class4)))

            if user1 == 1:
                self.user1class = self.class1
                print("Player 1 is a {} and he has {} hp.".format(self.user1class, self.class1hp))

            elif user1 == 2:
                self.user1class = self.class2
                print("Player 1 is a {} and he has {} hp.".format(self.user1class, self.class2hp))

            elif user1 == 3:
                self.user1class = self.class3
                print("Player 1 is a {} and he has {} hp.".format(self.user1class, self.class3hp))

            elif user1 == 4:
                self.user1class = self.class4
                print("Player 1 is a {} and he has {} hp.".format(self.user1class, self.class4hp))

            else:
                print("\nPlease enter an option from 1 to 4\n")
                self.user1pick()
        except:
            print("\nPlease enter an option from 1 to 4\n")
            self.user1pick()

    def user2pick(self):
        print("\nPlayer 2 turn\n")
        try:
            user2 = int(input("Choose which class you want to play: \n1) {}\n2) {}\n3) {} \n4) {} \n\n".
                              format(self.class1, self.class2, self.class3, self.class4)))

            if user2 == 1:
                self.user2class = self.class1
                print("Player 2 is a {} and he has {} hp.".format(self.user2class, self.class1hp))

            elif user2 == 2:
                self.user2class = self.class2
                print("Player 2 is a {} and he has {} hp.".format(self.user2class, self.class2hp))

            elif user2 == 3:
                self.user2class = self.class3
                print("Player 2 is a {} and he has {} hp.".format(self.user2class, self.class3hp))

            elif user2 == 4:
                self.user2class = self.class4
                print("Player 2 is a {} and he has {} hp.".format(self.user2class, self.class4hp))

            else:
                print("\nPlease enter an option from 1 to 4\n")
                self.user2pick()
        except:
            print("\nPlease enter an option from 1 to 4\n")
            self.user2pick()

    def Ceva(self):
        playeri = ["Player 1", "Player 2"]
        player_aleatoriu = random.choice(playeri)
        alegere = input(player_aleatoriu + " alege intre cap si pajura:  \n\n")
        lista = ['cap', 'pajura']
        rezultat = random.choice(lista)
        if alegere.lower() == rezultat.lower() and alegere.lower() in [x for x in lista]:
            print("Ai ghicit! {} incepe primul.".format(player_aleatoriu))
        elif alegere.lower() != rezultat.lower() and alegere.lower() in [x for x in lista]:
            print("Nu ai ghicit, {} incepe primul.".format(player_aleatoriu))
        else:
            print("Te rog alege intre cap si pajura!")

    def gameplay(self):
        pass

if __name__ == '__main__':
    Game()
